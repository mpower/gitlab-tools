FROM alpine

ARG BUILD_DATE
ARG VCS_REF
ARG BUILD_VERSION
ARG BUILD_BRANCH
ARG APP_DIR='/opt/gitlab-tools/'

LABEL MAINTAINER="max@maxtpower.com" \
org.label-schema.schema-version="1.0" \
org.label-schema.build-date=$BUILD_DATE \
org.label-schema.name="mpower/gitlab-tools" \
org.label-schema.description="GitLab Tools" \
org.label-schema.url="https://www.maxtpower.com" \
org.label-schema.vcs-url="https://gitlab.com/mpower/gitlab-tools" \
org.label-schema.vcs-ref=$VCS_REF \
org.label-schema.version=$BUILD_VERSION \
org.label-schema.vendor="Maxwell Power" \
org.label-schema.docker.cmd="docker run -it registry.gitlab.com/mpower/gitlab-tools/master bash"

RUN apk add bash curl jq bind-tools --no-cache
COPY . "${APP_DIR}"
WORKDIR "${APP_DIR}"
